#! /bin/sh
# /*@@
#  @file   flickcurl.sh
#  @date   2009-02-05
#  @author Erik Schnetter <schnetter@cct.lsu.edu.de>
#  @desc
#          Setup flickcurl as a thorn
#  @enddesc
# @@*/



# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
#      echo $ac_n "  Looking in $cctk_place""...$ac_c" #1>&6
      if test -r "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
#      echo "$ac_t"" No" #1>&6
    done

  return
}


# Work out where flickcurl is installed
if [ -z "$FLICKCURL_DIR" ]; then
  echo "BEGIN MESSAGE"
  echo 'FLICKCURL selected but no FLICKCURL_DIR set.  Checking some places...'
  echo "END MESSAGE"

  CCTK_Search FLICKCURL_DIR "/usr /usr/local /usr/local/flickcurl /usr/local/packages/flickcurl /usr/local/apps/flickcurl $HOME c:/packages/flickcurl" bin/flickcurl-config
  if [ -z "$FLICKCURL_DIR" ]; then
     echo "BEGIN ERROR" 
     echo 'Unable to locate the flickcurl directory -- please set FLICKCURL_DIR'
     echo "END ERROR" 
     exit 2
  fi
  echo "BEGIN MESSAGE"
  echo "Found a flickcurl package in $FLICKCURL_DIR"
  echo "END MESSAGE"
fi



FLICKCURL_INC_DIRS="$(${FLICKCURL_DIR}/bin/flickcurl-config --cflags | sed -e 's/^-I//g;s/ -I/ /g')"
FLICKCURL_LIB_DIRS="$(${FLICKCURL_DIR}/bin/flickcurl-config --libs   | sed -e 's/^-L//g;s/ -L/ /g')"
FLICKCURL_LIBS=''

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_FLICKCURL     = 1"
echo "FLICKCURL_DIR      = ${FLICKCURL_DIR}"
echo "FLICKCURL_INC_DIRS = ${FLICKCURL_INC_DIRS}"
echo "FLICKCURL_LIB_DIRS = ${FLICKCURL_LIB_DIRS}"
echo "FLICKCURL_LIBS     = ${FLICKCURL_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(FLICKCURL_INC_DIRS) $(CURL_INC_DIRS) $(LIBXML2_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(FLICKCURL_LIB_DIRS) $(CURL_LIB_DIRS) $(LIBXML2_LIB_DIRS)'
echo 'LIBRARY           $(FLICKCURL_LIBS) $(CURL_LIBS) $(LIBXML2_LIBS)'
